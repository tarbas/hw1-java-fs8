import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    public static boolean isNumeric(String str) {
        return str.matches("\\d+");
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random random = new Random();

        String toSmall = "Your number is too small. Please, try again.";
        String toBig = "Your number is too big. Please, try again.";

        ArrayList tryArr = new ArrayList();
        int secrInt = random.nextInt(100);

        System.out.println("Enter your name");
        String name = in.nextLine();
        System.out.println("Let the game begin!");
        System.out.println("Try to guess a secret number! Enter only positive integer!");

        do {
            String str;
            do {
                str = in.nextLine();
           }
            while (!isNumeric(str));

            int x = Integer.parseInt(str.trim());
            tryArr.add(x);

            if (x == secrInt) {
                System.out.println("Congratulations, " + name + ", secret number is: " + secrInt);
                Collections.sort(tryArr);
                System.out.println("Your numbers: ");
                for (Object i : tryArr) {
                    System.out.print(i+",");
                }
                break;
            }
            if (x < secrInt) System.out.println(toSmall);
            if (x > secrInt) System.out.println(toBig);
        }
        while (true);


    }
}
